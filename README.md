## FootballStadistics

> Developed by Jorge Martínez de la Chica
>
> Platform Java 1.8
>
> Database Sqlite (And all database compatible with JDBC)

This app is designed for that people that want to store an historical record of his favourites leagues for stadistical purposes. As is said it's implemented with Java and Java Swing, using JDBC as API for the database and sqlite as database. It's designed with the MVC approach which allow easy modifications.

For now is only in Spanish but is ready for changing it in the future.
