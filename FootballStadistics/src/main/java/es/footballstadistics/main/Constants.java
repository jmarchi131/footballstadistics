/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.footballstadistics.main;

/**
 *
 * @author jtort
 */
public interface Constants {
    public enum Ending{
        NULL('N'), WIN('G'), EVEN('X'), LOSE('P');
            
        public char state;
        
        private Ending(char state){
            this.state = state;
        }
        
        public char getState(){
            return this.state;
        }
    }
    
    public String URL = "./database.db";
}
