/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.footballstadistics.main;

import es.footballstadistics.main.Constants.Ending;
import static es.footballstadistics.main.Constants.Ending.WIN;
import static es.footballstadistics.main.Constants.Ending.EVEN;
import static es.footballstadistics.main.Constants.Ending.LOSE;

/**
 *
 * @author jtort
 */
public class Match {
    private Match lastMatch;
    private int score1;
    private int score2;
    private Ending ending1;
    private Ending ending2;
    private String team1;
    private String team2;
    
    private float winP;
    private float evenP;
    private float loseP;

    public Match() {
        lastMatch = null;
        score1 = 0;
        score2 = 0;
        ending1 = EVEN;
        ending2 = EVEN;
        team1 = "";
        team2 = "";
        
        winP = 0;
        evenP = 0;
        loseP = 0;
    }
    
    public Match(int score1, int score2, Ending ending1, Ending ending2, String team1, String team2) {
        lastMatch = null;
        this.score1 = score1;
        this.score2 = score2;
        this.ending1 = ending1;
        this.ending2 = ending2;
        this.team1 = team1;
        this.team2 = team2;
        
        winP = 0;
        evenP = 0;
        loseP = 0;
    }

    public int getScore1() {
        return score1;
    }

    public int getScore2() {
        return score2;
    }

    public Ending getEnding1() {
        return ending1;
    }

    public Ending getEnding2() {
        return ending2;
    }

    public void setScore(int score1, int score2){
        this.score1 = score1;
        this.score2 = score2;
        ending1 = getEnding(score1, score2)[0];
        ending2 = getEnding(score1, score2)[1];
    }
    
    public Match getLastMatch() {
        return lastMatch;
    }

    public void setLastMatch(Match lastMatch) {
        this.lastMatch = lastMatch;
    }

    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    public float getWinP() {
        return winP;
    }

    public void setWinP(float winP) {
        this.winP = winP;
    }

    public float getEvenP() {
        return evenP;
    }

    public void setEvenP(float evenP) {
        this.evenP = evenP;
    }

    public float getLoseP() {
        return loseP;
    }

    public void setLoseP(float loseP) {
        this.loseP = loseP;
    }
    
    public Ending[] getEnding(int score1, int score2){
        Ending data[] = new Ending[2];
        if(score1 > score2){
            data[0] = WIN;
            data[1] = LOSE;
        } else if (score2 > score1){
            data[0] = LOSE;
            data[1] = WIN;
        } else {
            data[0] = EVEN;
            data[1] = EVEN;
        }
        return data;
    }
}
