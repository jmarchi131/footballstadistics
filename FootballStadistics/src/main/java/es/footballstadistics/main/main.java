/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.footballstadistics.main;

import es.footballstadistics.UI.AddWindow;
import es.footballstadistics.UI.DeleteWindow;
import es.footballstadistics.UI.InitWindow;
import es.footballstadistics.UI.SearchWindow;
import es.footballstadistics.main.Constants.Ending;
import static es.footballstadistics.main.Constants.Ending.EVEN;
import static es.footballstadistics.main.Constants.Ending.LOSE;
import static es.footballstadistics.main.Constants.Ending.NULL;
import static es.footballstadistics.main.Constants.Ending.WIN;
import static es.footballstadistics.main.Constants.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author jtort
 */
public class main implements Runnable {

    private Database db;
    private JFrame gui;

    @Override
    public void run() {
        //Initialiting...
        db = new Database();
        if (db.Connect(URL)) {
            gui = new InitWindow(this);
            gui.setVisible(true);
        } else {
            //TODO: Crear JFrame de error
        }
    }

    public void openSearch() {
        gui.dispose();
        gui = new SearchWindow(this);
        gui.setVisible(true);
    }

    public void openInit() {
        gui.dispose();
        gui = new InitWindow(this);
        gui.setVisible(true);
    }

    public void openAdd() {
        gui.dispose();
        gui = new AddWindow(this);
        gui.setVisible(true);
    }
    
    public void openDelete(){
        gui.dispose();
        gui = new DeleteWindow(this);
        gui.setVisible(true);
    }
    
    public Ending[] calculateEnding(int score1, int score2){
        Ending[] ret = new Ending[2];
        if(score1 > score2){
            ret[0] = WIN;
            ret[1] = LOSE;
        } else if(score1 < score2){
            ret[0] = LOSE;
            ret[1] = WIN;
        } else {
            ret[0] = EVEN;
            ret[1] = EVEN;
        }
        return ret;
    }

    public List<String[]> searchFor(String e1S, String e2S, String score1S, String score2S, String t1, String t2, String fixtureS, String yearS, String divisionS, String league) {
        boolean filters = false;
        Ending e1;
        Ending e2;
        int score1;
        if (!score1S.equals("")) {
            score1 = Integer.parseInt(score1S);
        } else {
            score1 = -1;
        }
        int score2;
        if (!score2S.equals("")) {
            score2 = Integer.parseInt(score2S);
        } else {
            score2 = -1;
        }
        int fixture;
        if (!fixtureS.equals("")) {
            fixture = Integer.parseInt(fixtureS);
        } else {
            fixture = -1;
        }
        int year;
        if (!yearS.equals("")) {
            year = Integer.parseInt(yearS);
        } else {
            year = -1;
        }
        int division;
        if (!divisionS.equals("")) {
            division = Integer.parseInt(divisionS);
        } else {
            division = -1;
        }

        if (e1S.equals("G")) {
            e1 = WIN;
        } else if (e1S.equals("P")) {
            e1 = LOSE;
        } else if (e1S.equals("X")) {
            e1 = EVEN;
        } else {
            e1 = NULL;
        }
        if (e2S.equals("G")) {
            e2 = WIN;
        } else if (e2S.equals("P")) {
            e2 = LOSE;
        } else if (e2S.equals("X")) {
            e2 = EVEN;
        } else {
            e2 = NULL;
        }

        if (e1 != NULL) {
            filters = true;
        }
        if (e2 != NULL) {
            filters = true;
        }
        if (score1 != -1) {
            filters = true;
        }
        if (score2 != -1) {
            filters = true;
        }
        if (!t1.equals("")) {
            filters = true;
        }
        if (!t2.equals("")) {
            filters = true;
        }
        if (fixture != -1) {
            filters = true;
        }
        if (year != -1) {
            filters = true;
        }
        if (division != -1) {
            filters = true;
        }
        if (!league.equals("Todas")) {
            filters = true;
        }

        if (filters) {
            return db.selectFiltered(e1, e2, score1, score2, t1, t2, fixture, year, division, league);
        } else {
            return db.selectAllMatches();
        }
    }

    public List<String> getLeagues() {
        return db.getLeagues();
    }

    public List<String> getTeams() {
        return db.getTeams();
    }
    
    public List<String[]> getSeasons(){
        return db.getSeasons();
    }
    
    public List<String[]> getFixtures(){
        return db.getFixtures();
    } 
    
    public List<String> getBettingShops(){
        return db.getBettingShops();
    }
    
    public List<String[]> getBets(String score1S, String score2S, String team1, String team2, String fixtureNumberS, String fixtureYearS){
        int score1 = Integer.parseInt(score1S);
        int score2 = Integer.parseInt(score2S);
        int fixtureNumber = Integer.parseInt(fixtureNumberS);
        int fictureYear = Integer.parseInt(fixtureYearS);
        return db.getBets(score1, score2, team1, team2, fixtureNumber, fictureYear);
    }

    public void addTeam(String name, String league) {
        if (!name.equals("")) {
            if (!name.contains(";")) {
                if (db.addTeam(name, league)) {
                    JOptionPane.showMessageDialog(gui, "Equipo añadido", "Correcto", JOptionPane.PLAIN_MESSAGE);
                    System.out.println("OK");
                } else {
                    JOptionPane.showMessageDialog(gui, "Error al añadir el equipo", "Error", JOptionPane.ERROR_MESSAGE);
                    System.out.println("Nop");
                }
            }
        }
    }

    public void addLeague(String name) {
        if (!name.equals("")) {
            if (!name.contains(";")) {
                if (db.addLeague(name)) {
                    JOptionPane.showMessageDialog(gui, "Liga añadida", "Correcto", JOptionPane.PLAIN_MESSAGE);
                    System.out.println("OK");
                } else {
                    JOptionPane.showMessageDialog(gui, "Error al añadir la liga", "Error", JOptionPane.ERROR_MESSAGE);
                    System.out.println("Nop");
                }
            }
        }
    }

    public void addSeason(String yearS, String league) {
        int year;
        if (!yearS.equals("")) {
            year = Integer.parseInt(yearS);
            if (db.addSeason(year, league)) {
                JOptionPane.showMessageDialog(gui, "Temporada añadida", "Correcto", JOptionPane.PLAIN_MESSAGE);
                System.out.println("OK");
            } else {
                JOptionPane.showMessageDialog(gui, "Error al añadir la temporada", "Error", JOptionPane.ERROR_MESSAGE);
                System.out.println("Nop");
            }
        }
    }
    
    public void addFixture(String numberS, String day, String yearS, String league){
        if(!numberS.equals("") && !day.equals("")){
            int number = Integer.parseInt(numberS);
            int year = Integer.parseInt(yearS);
            if (db.addFixture(number, day, year, league)) {
                JOptionPane.showMessageDialog(gui, "Jornada añadida", "Correcto", JOptionPane.PLAIN_MESSAGE);
                System.out.println("OK");
            } else {
                JOptionPane.showMessageDialog(gui, "Error al añadir la jornada", "Error", JOptionPane.ERROR_MESSAGE);
                System.out.println("Nop");
            }
        }
    }
    
    public void addMatch(int pos1, int pos2, String t1, String t2, int r1, int r2, String fixYear, String fixNumber, String division){
        if(!division.equals("")){
            try{
                int number = Integer.parseInt(fixNumber);
                int year = Integer.parseInt(fixYear);
                Ending[] end = calculateEnding(r1, r2);
                if(db.addMatch(pos1, pos2, r1, r2, end[0].getState(), end[1].getState(), t1, t2, number, year, division)){
                    JOptionPane.showMessageDialog(gui, "Partido añadido", "Correcto", JOptionPane.PLAIN_MESSAGE);
                    System.out.println("OK");
                } else {
                    JOptionPane.showMessageDialog(gui, "Error al añadir el partido", "Error", JOptionPane.ERROR_MESSAGE);
                    System.out.println("Nop");
                }
            } catch (NumberFormatException ex){}
        }
    }

    public void addBettingShop(String name){
        if(!name.equals("")){
            if(db.addBettingShop(name)){
                JOptionPane.showMessageDialog(gui, "Casa de apuestas añadida", "Correcto", JOptionPane.PLAIN_MESSAGE);
                System.out.println("OK");
            } else {
                JOptionPane.showMessageDialog(gui, "Error al añadir la casa de apuestas", "Error", JOptionPane.ERROR_MESSAGE);
                System.out.println("Nop");
            }
        }
    }
    
    public void addBet(String bettingShop, int winP, int evenP, int loseP, String fixNumber, String fixYear, String teams){
        if(winP > -1 && winP < 101 && evenP > -1 && evenP < 101 && loseP > -1 && loseP < 101){
            Scanner sc = new Scanner(teams).useDelimiter(" - ");
            String team1 = sc.next();
            String team2 = sc.next();
            try{
                int fixtureNumber = Integer.parseInt(fixNumber);
                int fixtureYear = Integer.parseInt(fixYear);
                if(!team1.equals("") && !team2.equals("")){
                    if(db.addBet(bettingShop, winP, evenP, loseP, fixtureNumber, fixtureYear, team1, team2)){
                        JOptionPane.showMessageDialog(gui, "Porcentajes añadidos", "Correcto", JOptionPane.PLAIN_MESSAGE);
                        System.out.println("OK");
                    } else {
                        JOptionPane.showMessageDialog(gui, "Error al añadir los porcentajes", "Error", JOptionPane.ERROR_MESSAGE);
                        System.out.println("Nop");
                    }
                }
            }catch (NumberFormatException ex){}
        }
    }
    
    public void deleteRows(int table, List<String[]> rows){
        boolean error = false;
        switch(table){
            case 0: //Equipos
                for(String[] row : rows){
                    if(!db.deleteTeam(row[0])){
                        error = true;
                    }
                }
                if(error){
                    JOptionPane.showMessageDialog(gui, "Algún equipo no ha sido borrado correctamente", "Error", JOptionPane.ERROR_MESSAGE);
                    System.out.println("Algún equipo no ha sido borrado correctamente");
                } else {
                    JOptionPane.showMessageDialog(gui, "Borrado completado", "Correcto", JOptionPane.PLAIN_MESSAGE);
                    System.out.println("Borrado completado");
                }
                break;
            case 1: //Ligas
                for(String[] row : rows){
                    if(!db.deleteLeague(row[0])){
                        error = true;
                    }
                }
                if(error){
                    JOptionPane.showMessageDialog(gui, "Alguna liga no ha sido borrada correctamente", "Error", JOptionPane.ERROR_MESSAGE);
                    System.out.println("Algún equipo no ha sido borrado correctamente");
                } else {
                    JOptionPane.showMessageDialog(gui, "Borrado completado", "Correcto", JOptionPane.PLAIN_MESSAGE);
                    System.out.println("Borrado completado");
                }
                break;
            case 2: //Temporadas
                for(String[] row : rows){
                    if(!db.deleteSeason(Integer.parseInt(row[0]), row[1])){
                        error = true;
                    }
                }
                if(error){
                    JOptionPane.showMessageDialog(gui, "Alguna temporada no ha sido borrada correctamente", "Error", JOptionPane.ERROR_MESSAGE);
                    System.out.println("Algún equipo no ha sido borrado correctamente");
                } else {
                    JOptionPane.showMessageDialog(gui, "Borrado completado", "Correcto", JOptionPane.PLAIN_MESSAGE);
                    System.out.println("Borrado completado");
                }
                break;
            case 3: //Jornadas
                for(String[] row : rows){
                    if(!db.deleteFixture(Integer.parseInt(row[0]), Integer.parseInt(row[1]), row[2])){
                        error = true;
                    }
                }
                if(error){
                    JOptionPane.showMessageDialog(gui, "Alguna jornada no ha sido borrada correctamente", "Error", JOptionPane.ERROR_MESSAGE);
                    System.out.println("Algún equipo no ha sido borrado correctamente");
                } else {
                    JOptionPane.showMessageDialog(gui, "Borrado completado", "Correcto", JOptionPane.PLAIN_MESSAGE);
                    System.out.println("Borrado completado");
                }
                break;
            case 4: //Casas de Apuestas
                for(String[] row : rows){
                    if(!db.deleteBettingShop(row[0])){
                        error = true;
                    }
                }
                if(error){
                    JOptionPane.showMessageDialog(gui, "Alguna casa de apuestas no ha sido borrada correctamente", "Error", JOptionPane.ERROR_MESSAGE);
                    System.out.println("Algún equipo no ha sido borrado correctamente");
                } else {
                    JOptionPane.showMessageDialog(gui, "Borrado completado", "Correcto", JOptionPane.PLAIN_MESSAGE);
                    System.out.println("Borrado completado");
                }
                break;
            case 5: //Partidos
                for(String[] row : rows){
                    if(!db.deleteMatch(Integer.parseInt(row[2]), Integer.parseInt(row[3]), row[4], row[5], Integer.parseInt(row[6]), Integer.parseInt(row[7]))){
                        error = true;
                    }
                }
                if(error){
                    JOptionPane.showMessageDialog(gui, "Algún partido no ha sido borrado correctamente", "Error", JOptionPane.ERROR_MESSAGE);
                    System.out.println("Algún equipo no ha sido borrado correctamente");
                } else {
                    JOptionPane.showMessageDialog(gui, "Borrado completado", "Correcto", JOptionPane.PLAIN_MESSAGE);
                    System.out.println("Borrado completado");
                }
                break;
            case 6: //Apuestas
                break;
            default:
                for(String[] row : rows){
                    System.out.println(Arrays.toString(row));
                }
                break;
        }
    }
    
    public void editRow(String[] origin, String[] destiny){
        if(db.editRow(origin, destiny)){
            JOptionPane.showMessageDialog(gui, "Partido Editado", "Correcto", JOptionPane.PLAIN_MESSAGE);
            System.err.println("Aleluya");
        } else {
            JOptionPane.showMessageDialog(gui, "Error al editar el partido", "Error", JOptionPane.ERROR_MESSAGE);
            System.out.println("FFFFFF");
        }
    }
}
