/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.footballstadistics.main;

import es.footballstadistics.main.Constants.Ending;
import static es.footballstadistics.main.Constants.Ending.EVEN;
import static es.footballstadistics.main.Constants.Ending.LOSE;
import static es.footballstadistics.main.Constants.Ending.NULL;
import static es.footballstadistics.main.Constants.Ending.WIN;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jtort
 */
public class Database {

    private Connection connect;

    public boolean Connect(String url) {
        try {
            connect = DriverManager.getConnection("jdbc:sqlite:" + url);
            return true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    public boolean Disconnect() {
        if (connect != null) {
            try {
                connect.close();
                return true;
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return false;
            }
        }
        return true;
    }

    public List<String[]> selectAllMatches() {
        List<String[]> rest = new ArrayList<>();
        if (connect != null) {
            try {
                Statement st = connect.createStatement();
                String query = "SELECT match.id as id, score1, score2, t1.name as name1, t2.name as name2, number, seasons.year as year "//, AVG(percentage.win) as win, AVG(percentage.even) as even, AVG(percentage.lose) as lose "
                        + "FROM match "
                        + "INNER JOIN fixture on fixture.id = match.id_fixture "
                        + "INNER JOIN seasons on seasons.id = fixture.idSeason "
                        + "INNER JOIN teams as t1 on t1.id = match.team1 "
                        + "INNER JOIN teams as t2 on t2.id = match.team2 "
                        + "ORDER BY year DESC, number DESC";

                ResultSet rs = st.executeQuery(query);

                while (rs.next()) {
                    String datos[] = new String[11];
                    datos[2] = rs.getString("score1");
                    datos[3] = rs.getString("score2");
                    datos[4] = rs.getString("name1");
                    datos[5] = rs.getString("name2");
                    datos[6] = rs.getString("number");
                    datos[7] = rs.getString("year");
                    Statement st2 = connect.createStatement();
                    String query2 = "SELECT ending1, ending2, t1.name as name1, t2.name as name2 "
                            + "FROM match "
                            + "INNER JOIN fixture on fixture.id = match.id_fixture "
                            + "INNER JOIN teams as t1 on t1.id = match.team1 "
                            + "INNER JOIN teams as t2 on t2.id = match.team2 "
                            + "WHERE fixture.number = " + (Integer.parseInt(datos[6]) - 1)
                            + " AND (name1 LIKE \"" + datos[4] + "\" OR name2 LIKE \"" + datos[4] + "\")";

                    ResultSet rs2 = st2.executeQuery(query2);
                    if (rs2.next()) {
                        if (datos[4].equals(rs2.getString("name1"))) {
                            datos[0] = rs2.getString("ending1");
                        } else {
                            datos[0] = rs2.getString("ending2");
                        }
                    } else {
                        datos[0] = "";
                    }
                    query2 = "SELECT ending1, ending2, t1.name as name1, t2.name as name2 "
                            + "FROM match "
                            + "INNER JOIN fixture on fixture.id = match.id_fixture "
                            + "INNER JOIN teams as t1 on t1.id = match.team1 "
                            + "INNER JOIN teams as t2 on t2.id = match.team2 "
                            + "WHERE fixture.number = " + (Integer.parseInt(datos[6]) - 1)
                            + " AND (name1 LIKE \"" + datos[5] + "\" OR name2 LIKE \"" + datos[5] + "\")";
                    rs2 = st2.executeQuery(query2);
                    if (rs2.next()) {
                        if (datos[5].equals(rs2.getString("name1"))) {
                            datos[1] = rs2.getString("ending1");
                        } else {
                            datos[1] = rs2.getString("ending2");
                        }
                    } else {
                        datos[1] = "";
                    }
                    
                    query2 = "SELECT AVG(win) as winM, AVG(even) as evenM, AVG(lose) as loseM FROM percentage WHERE id_Match = " + rs.getString("id");
                    rs2 = st2.executeQuery(query2);
                    if(rs2.next()){
                        datos[8] = rs2.getString("winM");
                        datos[9] = rs2.getString("evenM");
                        datos[10] = rs2.getString("loseM");
                    }
                    
                    rest.add(datos);
                }
            } catch (SQLException ex) {
                System.out.println("Error con la consulta (en partidos sin filtro): " + ex.getMessage());
            }
        }
        return rest;
    }

    public List<String[]> selectFiltered(Ending e1, Ending e2, int score1, int score2, String t1, String t2, int fixture, int year, int division, String league) {
        List<String[]> rest = new ArrayList<>();
        boolean filters[] = new boolean[8];
        boolean globalFilters = false;
        if (connect != null) {
            try {
                Statement st = connect.createStatement();
                String query = "SELECT match.id as id, score1, score2, t1.name as name1, t2.name as name2, number, seasons.year as year , division.div as division, leagues.name as league "
                        + "FROM match "
                        + "INNER JOIN fixture on fixture.id = match.id_fixture "
                        + "INNER JOIN seasons on seasons.id = fixture.idSeason "
                        + "INNER JOIN teams as t1 on t1.id = match.team1 "
                        + "INNER JOIN teams as t2 on t2.id = match.team2 "
                        + "INNER JOIN division on (division.id_Team = match.team2) AND division.idSeason = seasons.id "
                        + "INNER JOIN leagues on leagues.id = fixture.id_League";

                //Checking filters
                boolean first = true;
                if (score1 != -1) {
                    query += " WHERE score1 = " + score1;
                    first = false;
                }
                if (score2 != -1) {
                    if (!first) {
                        query += " AND";
                    } else {
                        first = false;
                        query += " WHERE";
                    }
                    query += " score2 = " + score2;
                }
                if (!t1.equals("")) {
                    if (!first) {
                        query += " AND";
                    } else {
                        first = false;
                        query += " WHERE";
                    }
                    String aux = "SELECT id FROM teams WHERE name LIKE \"" + t1 + "\"";
                    Statement auxSt = connect.createStatement();
                    ResultSet auxRs = auxSt.executeQuery(aux);
                    if(auxRs.next())
                        query += " t1.id = " + auxRs.getString("id");
                }
                if (!t2.equals("")) {
                    if (!first) {
                        query += " AND";
                    } else {
                        first = false;
                        query += " WHERE";
                    }
                    query += " name2 LIKE \"" + t2 + "\"";
                }
                if (fixture != -1) {
                    if (!first) {
                        query += " AND";
                    } else {
                        first = false;
                        query += " WHERE";
                    }
                    query += " number = " + fixture;
                }
                if (year != -1) {
                    if (!first) {
                        query += " AND";
                    } else {
                        first = false;
                        query += " WHERE";
                    }
                    query += " seasons.year = " + year;
                }
                
                if (division != -1) {
                    if (!first) {
                        query += " AND";
                    } else {
                        first = false;
                        query += " WHERE";
                    }
                    query += " division.div = \"" + division + "\"";
                }
                if (!league.equals("Todas")) {
                    if (!first) {
                        query += " AND";
                    } else {
                        first = false;
                        query += " WHERE";
                    }
                    query += " league LIKE \"" + league + "\"";
                }
                query += " GROUP BY match.id ORDER BY year DESC, number DESC";
                ResultSet rs = st.executeQuery(query);

                while (rs.next()) {
                    String datos[] = new String[11];
                    datos[2] = rs.getString("score1");
                    datos[3] = rs.getString("score2");
                    datos[4] = rs.getString("name1");
                    datos[5] = rs.getString("name2");
                    datos[6] = rs.getString("number");
                    datos[7] = rs.getString("year");
                    
                    Statement st2 = connect.createStatement();
                    String query2 = "SELECT ending1, ending2, t1.name as name1, t2.name as name2 "
                            + "FROM match "
                            + "INNER JOIN fixture on fixture.id = match.id_fixture "
                            + "INNER JOIN teams as t1 on t1.id = match.team1 "
                            + "INNER JOIN teams as t2 on t2.id = match.team2 "
                            + "WHERE fixture.number = " + (Integer.parseInt(datos[6]) - 1)
                            + " AND (name1 LIKE \"" + datos[4] + "\" OR name2 LIKE \"" + datos[4] + "\")";
                    /*
                    if (e1 != NULL) {
                        query2 += " AND ending1 LIKE \"" + e1.getState() + "\"";
                    }
                    if (e2 != NULL) {
                        query2 += " AND ending2 LIKE \"" + e2.getState() + "\"";
                    }
                    */
                    ResultSet rs2 = st2.executeQuery(query2);
                    if (rs2.next()) {
                        if (datos[4].equals(rs2.getString("name1"))) {
                            datos[0] = rs2.getString("ending1");
                        } else {
                            datos[0] = rs2.getString("ending2");
                        }
                    } else {
                        datos[0] = "";
                    }
                    query2 = "SELECT ending1, ending2, t1.name as name1, t2.name as name2 "
                            + "FROM match "
                            + "INNER JOIN fixture on fixture.id = match.id_fixture "
                            + "INNER JOIN teams as t1 on t1.id = match.team1 "
                            + "INNER JOIN teams as t2 on t2.id = match.team2 "
                            + "WHERE fixture.number = " + (Integer.parseInt(datos[6]) - 1)
                            + " AND (name1 LIKE \"" + datos[5] + "\" OR name2 LIKE \"" + datos[5] + "\")";
                    /*
                    if (e1 != NULL) {
                        query2 += " AND ending1 LIKE \"" + e1.getState() + "\"";
                    }
                    if (e2 != NULL) {
                        query2 += " AND ending2 LIKE \"" + e2.getState() + "\"";
                    }
                    */
                    rs2 = st2.executeQuery(query2);
                    if (rs2.next()) {
                        if (datos[5].equals(rs2.getString("name1"))) {
                            datos[1] = rs2.getString("ending1");
                        } else {
                            datos[1] = rs2.getString("ending2");
                        }
                    } else {
                        datos[1] = "";
                    }

                    query2 = "SELECT AVG(win) as winM, AVG(even) as evenM, AVG(lose) as loseM FROM percentage WHERE id_Match = " + rs.getString("id");
                    rs2 = st2.executeQuery(query2);
                    if(rs2.next()){
                        datos[8] = rs2.getString("winM");
                        datos[9] = rs2.getString("evenM");
                        datos[10] = rs2.getString("loseM");
                    }
                    
                    if(e1 == NULL && e2 == NULL){
                        rest.add(datos);
                    } else if(e1 != NULL && e2 == NULL){
                        if(datos[0].equals(e1.state + "")){
                            rest.add(datos);
                        }
                    } else if(e1 == NULL && e2 != NULL){
                        if(datos[1].equals(e2.state + "")){
                            rest.add(datos);
                        }
                    } else {
                        if(datos[0].equals(e1.state + "") && datos[1].equals(e2.state + "")){
                            rest.add(datos);
                        }
                    }
                }
            } catch (SQLException ex) {
                System.out.println("Error con la consulta (en partidos filtrados): " + ex.getMessage());
            }
        }
        return rest;
    }
    
    public List<String> getLeagues(){
        List<String> leagues = new ArrayList<>();
        
        if (connect != null) {
            try {
                Statement st = connect.createStatement();
                String query = "SELECT name FROM leagues";
                ResultSet rs = st.executeQuery(query);
                while(rs.next()){
                    leagues.add(rs.getString("name"));
                }
            } catch (SQLException ex) {
                System.out.println("Error con la consulta (en ligas): " + ex.getMessage());
            }
        }
        return leagues;
    }
    
    public List<String> getTeams(){
        List<String> teams = new ArrayList<>();
        
        if (connect != null) {
            try {
                Statement st = connect.createStatement();
                String query = "SELECT name FROM teams";
                ResultSet rs = st.executeQuery(query);
                while(rs.next()){
                    teams.add(rs.getString("name"));
                }
            } catch (SQLException ex) {
                System.out.println("Error con la consulta (en equipos): " + ex.getMessage());
            }
        }
        return teams;
    }
    
    public List<String[]> getSeasons(){
        List<String[]> seasons = new ArrayList<>();
        
        if (connect != null) {
            try {
                Statement st = connect.createStatement();
                String query = "SELECT seasons.year as year, leagues.name as name FROM seasons "
                            + "INNER JOIN leagues on seasons.id_League = leagues.id "
                            + "ORDER BY year DESC";
                ResultSet rs = st.executeQuery(query);
                while(rs.next()){
                    String[] s = new String[2];
                    s[0] = rs.getString("year");
                    s[1] = rs.getString("name");
                    seasons.add(s);
                }
            } catch (SQLException ex) {
                System.out.println("Error con la consulta (en temporadas): " + ex.getMessage());
            }
        }
        
        return seasons;
    }
    
    public List<String[]> getFixtures(){
        List<String[]> fixtures = new ArrayList<>();
        
        if (connect != null) {
            try {
                Statement st = connect.createStatement();
                String query = "SELECT fixture.id as id, number, seasons.year as year, leagues.name as name FROM fixture "
                            + "INNER JOIN leagues on fixture.id_League = leagues.id "
                            + "INNER JOIN seasons on seasons.id = fixture.idSeason "
                            + "ORDER BY year DESC, number ASC";
                ResultSet rs = st.executeQuery(query);
                while(rs.next()){
                    String[] s = new String[3];
                    s[0] = rs.getString("number");
                    s[1] = rs.getString("year");
                    s[2] = rs.getString("name");
                    fixtures.add(s);
                }
            } catch (SQLException ex) {
                System.out.println("Error con la consulta (en jornadas): " + ex.getMessage());
            }
        }
        
        return fixtures;
    }
    
    public List<String> getBettingShops(){
        List<String> shops = new ArrayList<>();
        
        if(connect != null){
            try{
                String query = "SELECT name FROM betting_shops";
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery(query);
                while(rs.next()){
                    shops.add(rs.getString("name"));
                }
            } catch (SQLException ex){
                System.out.println("Error con la consulta (en casas): " + ex.getMessage());
            }
        }
        
        return shops;
    }
      
    public List<String[]> getBets(int score1, int score2, String team1, String team2, int fixtureNumber, int fixtureYear){
        List<String[]> percentages = new ArrayList<String[]>();
        if(connect != null){
            try{
                String idT1 = null;
                String idT2 = null;
                String idLeague = null;
                String idSeason = null;
                String idFixture = null;
                String idMatch = null;
                Statement st = connect.createStatement();

                String query = "SELECT id, id_League FROM teams WHERE name LIKE \"" + team1 + "\"";
                ResultSet rs = st.executeQuery(query);
                if(rs.next()){
                    idT1 = rs.getString("id");
                    idLeague = rs.getString("id_League");
                }
                query = "SELECT id, id_League FROM teams WHERE name LIKE \"" + team2 + "\"";
                rs = st.executeQuery(query);
                if(rs.next()){
                    idT2 = rs.getString("id");
                    if(!idLeague.equals(rs.getString("id_League")) && !idLeague.equals(null)){
                        throw new NullPointerException();
                    }
                }
                query = "SELECT id FROM seasons WHERE year = " + fixtureYear;
                rs = st.executeQuery(query);
                if(rs.next()){
                    idSeason = rs.getString("id");
                }
                query = "SELECT id FROM fixture WHERE number = " + fixtureNumber + " AND idSeason = " + idSeason + " AND id_League = " + idLeague;
                rs = st.executeQuery(query);
                if(rs.next()){
                    idFixture = rs.getString("id");
                }
                
                query = "SELECT id FROM match WHERE score1 = " + score1 + " AND score2 = " + score2 + " AND id_fixture = " + idFixture + " AND team1 = " + idT1 + " AND team2 = " + idT2;
                rs = st.executeQuery(query);
                if(rs.next()){
                    idMatch = rs.getString("id");
                }
                
                query = "SELECT name, win, even, lose FROM percentage "
                        + "INNER JOIN betting_shops on betting_shops.id = id_Betting "
                        + "WHERE id_Match = " + idMatch;
                rs = st.executeQuery(query);
                while(rs.next()){
                    String[] data = new String[4];
                    data[0] = rs.getString("name");
                    data[1] = rs.getString("win");
                    data[2] = rs.getString("even");
                    data[3] = rs.getString("lose");
                    percentages.add(data);
                }
                
            } catch(NullPointerException|SQLException ex){}
        }
        return percentages;
    }
    
    public boolean addTeam(String name, String league){
        if(connect != null){
            try{
                String quest;
                Statement st = connect.createStatement();
                
                quest = "SELECT id FROM leagues WHERE name LIKE \"" + league + "\"";
                ResultSet rs = st.executeQuery(quest);
                int idLeague;
                if(rs.next()){
                    try {
                        idLeague = Integer.parseInt(rs.getString("id"));
                        String query = "INSERT INTO teams(id_League, name) VALUES(" + idLeague + ", \"" + name + "\")";
                        st.executeUpdate(query);
                        return true;
                    } catch(NullPointerException ex){
                    }
                }
            } catch(SQLException ex){
                System.out.println("Error en la BBDD:" + ex.getMessage());
            }
        }
        return false;
    }
    
    public boolean deleteTeam(String name){
        if(connect != null){
            try{
                String query = "PRAGMA foreign_keys = ON;"
                        + "DELETE FROM teams WHERE name LIKE \"" + name + "\"";
                Statement st = connect.createStatement();
                st.executeUpdate(query);
                return true;
            } catch(SQLException ex){
                System.out.println("Error en el borrado (de equipos): " + ex.getMessage());
            }
        }
        return false;
    }
    
    public boolean addLeague(String name){
        if(connect != null){
            try{
                Statement st = connect.createStatement();
                String query = "INSERT INTO leagues(name) VALUES(" + "\"" + name + "\")";
                st.executeUpdate(query);
                return true;
            } catch(SQLException ex){
                System.out.println("Error en la BBDD:" + ex.getMessage());
            }
        }
        return false;
    }
    
    public boolean deleteLeague(String name){
        if(connect != null){
            try{
                String query = "PRAGMA foreign_keys = ON;"
                        + "DELETE FROM leagues WHERE name LIKE \"" + name + "\"";
                Statement st = connect.createStatement();
                st.executeUpdate(query);
                return true;
            } catch(SQLException ex){}
        }
        return false;
    }
    
    public boolean addSeason(int year, String league){
        if(connect != null){
            try{
                String query = "SELECT id FROM leagues WHERE name LIKE \"" + league + "\"";
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery(query);
                if(rs.next()){
                    String update = "INSERT INTO seasons(year,id_League) VALUES("+ year + "," + rs.getString("id") + ")";
                    st.executeUpdate(update);
                    return true;
                }
            } catch(SQLException ex){
                System.out.println("Error en la BBDD: " + ex.getMessage());
            }
        }
        return false;
    }
    
    public boolean deleteSeason(int year, String league){
        if(connect != null){
            try{
                String query = "SELECT id FROM leagues WHERE name LIKE \"" + league + "\"";
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery(query);
                if(rs.next()){
                    String update = "PRAGMA foreign_keys = ON;"
                            + "DELETE FROM seasons WHERE year = "+ year + " AND id_League = " + rs.getString("id");
                    st.executeUpdate(update);
                    return true;
                }
            } catch(SQLException ex){}
        }
        return false;
    }
    
    public boolean addFixture(int number, String day, int year, String league){
        if(connect != null){
            try{
                String query = "SELECT id FROM seasons WHERE year = " + year;
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery(query);
                String idSeason = null;
                if(rs.next()){
                   idSeason = rs.getString("id");
                }
                query = "SELECT id FROM leagues WHERE leagues.name LIKE \"" + league + "\"";
                st = connect.createStatement();
                rs = st.executeQuery(query);
                
                if(rs.next()){
                    String update = "INSERT INTO fixture(number, match_day, idSeason, id_League) VALUES(" + number + ", " + day + ", " 
                                    + idSeason + "," + rs.getString("id") + ")";
                    st.executeUpdate(update);
                    return true;
                }
            } catch(SQLException ex){
                System.out.println("Error en la BBDD: " + ex.getMessage());
            } catch(NullPointerException ex){}
        }
        return false;
    }
    
    public boolean deleteFixture(int number, int year, String league){
        if(connect != null){
            try{
                String idSeason = null;
                String query = "SELECT id FROM seasons WHERE year = " + year;
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery(query);
                if(rs.next()){
                    idSeason = rs.getString("id");
                }
                query = "SELECT id FROM leagues WHERE leagues.name LIKE \"" + league + "\"";
                st = connect.createStatement();
                rs = st.executeQuery(query);
                
                if(rs.next()){
                    String update = "PRAGMA foreign_keys = ON;"
                            + "DELETE FROM fixture WHERE number = " + number + " AND  idSeason = " + idSeason + " AND id_League = " + rs.getString("id");
                    st.executeUpdate(update);
                    return true;
                }
            } catch(SQLException ex){
                System.out.println("Error en la BBDD: " + ex.getMessage());
            }
        }
        return false;
    }
    
    public boolean addMatch(int pos1, int pos2, int score1, int score2, char e1, char e2, String team1, String team2, int fixtureNumber, int fixtureYear, String division){
        if(connect != null){
            try{
                String idT1 = null;
                String idT2 = null;
                String idLeague = null;
                String idSeason = null;
                String idFixture = null;
                Statement st = connect.createStatement();

                String query = "SELECT id, id_League FROM teams WHERE name LIKE \"" + team1 + "\"";
                ResultSet rs = st.executeQuery(query);
                if(rs.next()){
                    idT1 = rs.getString("id");
                    idLeague = rs.getString("id_League");
                }
                query = "SELECT id, id_League FROM teams WHERE name LIKE \"" + team2 + "\"";
                rs = st.executeQuery(query);
                if(rs.next()){
                    idT2 = rs.getString("id");
                    if(!idLeague.equals(rs.getString("id_League")) && !idLeague.equals(null)){
                        throw new NullPointerException();
                    }
                }
                query = "SELECT id FROM seasons WHERE year = " + fixtureYear;
                rs = st.executeQuery(query);
                if(rs.next()){
                    idSeason = rs.getString("id");
                }
                query = "SELECT id FROM fixture WHERE number = " + fixtureNumber + " AND idSeason = " + idSeason + " AND id_League = " + idLeague;
                rs = st.executeQuery(query);
                if(rs.next()){
                    idFixture = rs.getString("id");
                }
                
                query = "INSERT INTO match(score1, score2, ending1, ending2, id_fixture, team1, team2) VALUES (" + score1 + ", " + score2 + ", \"" + e1 + "\", \"" + e2 + "\", " + idFixture + ", " + idT1 + ", " + idT2 + ")";
                st.executeUpdate(query);
                
                query = "INSERT INTO team_fixture VALUES (" + pos1 + ", " + idT1 + ", " + idFixture + ")";
                st.executeUpdate(query);
                query = "INSERT INTO team_fixture VALUES (" + pos2 + ", " + idT2 + ", " + idFixture + ")"; 
                st.executeUpdate(query);
                
                query = "SELECT div FROM division WHERE id_Team = " + idT1 + " AND idSeason = " + idSeason;
                rs = st.executeQuery(query);
                if(!rs.next()){
                    String query2 = "INSERT INTO division VALUES(\"" + division + "\", " + idT1 + ", " + idSeason + ")";
                    st.executeUpdate(query2);
                }
                query = "SELECT div FROM division WHERE id_Team = " + idT2 + " AND idSeason = " + idSeason;
                rs = st.executeQuery(query);
                if(!rs.next()){
                    String query2 = "INSERT INTO division VALUES(\"" + division + "\", " + idT2 + ", " + idSeason + ")";
                    st.executeUpdate(query2);
                }
                
                return true;                
            } catch(NullPointerException|SQLException ex){
                 return false;   
            }
        }
        return false;
    }
    
    private boolean addMatch(String id, int pos1, int pos2, int score1, int score2, char e1, char e2, String team1, String team2, int fixtureNumber, int fixtureYear, String division){
        if(connect != null){
            try{
                String idT1 = null;
                String idT2 = null;
                String idLeague = null;
                String idSeason = null;
                String idFixture = null;
                Statement st = connect.createStatement();

                String query = "SELECT id, id_League FROM teams WHERE name LIKE \"" + team1 + "\"";
                ResultSet rs = st.executeQuery(query);
                if(rs.next()){
                    idT1 = rs.getString("id");
                    idLeague = rs.getString("id_League");
                }
                query = "SELECT id, id_League FROM teams WHERE name LIKE \"" + team2 + "\"";
                rs = st.executeQuery(query);
                if(rs.next()){
                    idT2 = rs.getString("id");
                    if(!idLeague.equals(rs.getString("id_League")) && !idLeague.equals(null)){
                        throw new NullPointerException();
                    }
                }
                query = "SELECT id FROM seasons WHERE year = " + fixtureYear;
                rs = st.executeQuery(query);
                if(rs.next()){
                    idSeason = rs.getString("id");
                }
                query = "SELECT id FROM fixture WHERE number = " + fixtureNumber + " AND idSeason = " + idSeason + " AND id_League = " + idLeague;
                rs = st.executeQuery(query);
                if(rs.next()){
                    idFixture = rs.getString("id");
                }
                
                query = "INSERT INTO match(id, score1, score2, ending1, ending2, id_fixture, team1, team2) VALUES (" + id + ", " + score1 + ", " + score2 + ", \"" + e1 + "\", \"" + e2 + "\", " + idFixture + ", " + idT1 + ", " + idT2 + ")";
                st.executeUpdate(query);
                
                query = "INSERT INTO team_fixture VALUES (" + pos1 + ", " + idT1 + ", " + idFixture + ")";
                st.executeUpdate(query);
                query = "INSERT INTO team_fixture VALUES (" + pos2 + ", " + idT2 + ", " + idFixture + ")"; 
                st.executeUpdate(query);
                
                query = "SELECT div FROM division WHERE id_Team = " + idT1 + " AND idSeason = " + idSeason;
                rs = st.executeQuery(query);
                if(!rs.next()){
                    String query2 = "INSERT INTO division VALUES(\"" + division + "\", " + idT1 + ", " + idSeason + ")";
                    st.executeUpdate(query2);
                }
                query = "SELECT div FROM division WHERE id_Team = " + idT2 + " AND idSeason = " + idSeason;
                rs = st.executeQuery(query);
                if(!rs.next()){
                    String query2 = "INSERT INTO division VALUES(\"" + division + "\", " + idT2 + ", " + idSeason + ")";
                    st.executeUpdate(query2);
                }
                
                return true;                
            } catch(NullPointerException|SQLException ex){
                 return false;   
            }
        }
        return false;
    }
    
    public boolean deleteMatch(int score1, int score2, String team1, String team2, int fixtureNumber, int fixtureYear){
        if(connect != null){
            try{
                String idT1 = null;
                String idT2 = null;
                String idLeague = null;
                String idSeason = null;
                String idFixture = null;
                Statement st = connect.createStatement();

                String query = "SELECT id, id_League FROM teams WHERE name LIKE \"" + team1 + "\"";
                ResultSet rs = st.executeQuery(query);
                if(rs.next()){
                    idT1 = rs.getString("id");
                    idLeague = rs.getString("id_League");
                }
                query = "SELECT id, id_League FROM teams WHERE name LIKE \"" + team2 + "\"";
                rs = st.executeQuery(query);
                if(rs.next()){
                    idT2 = rs.getString("id");
                    if(!idLeague.equals(rs.getString("id_League")) && !idLeague.equals(null)){
                        throw new NullPointerException();
                    }
                }
                query = "SELECT id FROM seasons WHERE year = " + fixtureYear;
                rs = st.executeQuery(query);
                if(rs.next()){
                    idSeason = rs.getString("id");
                }
                query = "SELECT id FROM fixture WHERE number = " + fixtureNumber + " AND idSeason = " + idSeason + " AND id_League = " + idLeague;
                rs = st.executeQuery(query);
                if(rs.next()){
                    idFixture = rs.getString("id");
                }
                
                query = "PRAGMA foreign_keys = ON;"
                        + "DELETE FROM match WHERE score1 = " + score1 + " AND score2 = " + score2 + " AND id_fixture = " + idFixture + " AND team1 = " + idT1 + " AND team2 = " + idT2;
                st.executeUpdate(query);
                
                query = "PRAGMA foreign_keys = ON;"
                        + "DELETE FROM team_fixture WHERE id_Team = " + idT1 + " AND id_fixture = " + idFixture;
                st.executeUpdate(query);
                query = "PRAGMA foreign_keys = ON;"
                        + "DELETE FROM team_fixture WHERE id_Team = " + idT2 + " AND id_fixture = " + idFixture; 
                st.executeUpdate(query);
                
                query = "SELECT id_fixture FROM match WHERE team1 = " + idT1 + " OR team2 = " + idT1;
                rs = st.executeQuery(query);
                if(!rs.next()){
                    query = "PRAGMA foreign_keys = ON;"
                            + "DELETE FROM division WHERE id_Team = " + idT1 + " AND idSeason = " + idSeason;
                    st.executeUpdate(query);
                }
                query = "SELECT id_fixture FROM match WHERE team1 = " + idT2 + " OR team2 = " + idT2;
                rs = st.executeQuery(query);
                if(!rs.next()){
                    query = "PRAGMA foreign_keys = ON;"
                            + "DELETE FROM division WHERE id_Team = " + idT2 + " AND idSeason = " + idSeason;
                    st.executeUpdate(query);
                }
                
                return true;
            } catch(NullPointerException|SQLException ex){
                System.out.println(ex.getMessage());
            }
        }
        return false;
    }
    
    public boolean addBet(String bettingShop, int winP, int evenP, int loseP, int fixNumber, int fixYear, String team1, String team2){
        if(connect != null){
            try{
                String idBetting = null;
                String idFixture = null;
                String idT1 = null;
                String idT2 = null;
                String idLeague = null;
                String idSeason = null;
                String idMatch = null;
                Statement st = connect.createStatement();
                
                String query = "SELECT id FROM betting_shops WHERE name LIKE \"" + bettingShop + "\"";
                ResultSet rs = st.executeQuery(query);
                if(rs.next()){
                    idBetting = rs.getString("id");
                } else {
                    throw new InternalError();
                }
                query = "SELECT id FROM seasons WHERE year = " + fixYear;
                rs = st.executeQuery(query);
                if(rs.next()){
                    idSeason = rs.getString("id");
                }
                query = "SELECT id, id_League FROM teams WHERE name LIKE \"" + team1 + "\"";
                rs = st.executeQuery(query);
                if(rs.next()){
                    idT1 = rs.getString("id");
                    idLeague = rs.getString("id_League");
                } else {
                    throw new InternalError();
                }
                query = "SELECT id FROM teams WHERE name LIKE \"" + team2 + "\"";
                rs = st.executeQuery(query);
                if(rs.next()){
                    idT2 = rs.getString("id");
                } else {
                    throw new InternalError();
                }
                
                query = "SELECT id FROM fixture WHERE number = " + fixNumber + " AND idSeason = " + idSeason + " AND id_League = " + idLeague;
                rs = st.executeQuery(query);
                if(rs.next()){
                    idFixture = rs.getString("id");
                } else {
                    throw new InternalError();
                }
                
                query = "SELECT id FROM match WHERE id_fixture = " + idFixture + " AND team1 = " + idT1 + " AND team2 = " + idT2;
                rs = st.executeQuery(query);
                if(rs.next()){
                    idMatch = rs.getString("id");
                } else {
                    throw new InternalError();
                }
                
                if(idBetting != null && idMatch != null){
                    query = "INSERT INTO percentage VALUES (" + winP + ", " + evenP + ", " + loseP + ", " + idBetting + ", " + idMatch + ")";
                    st.executeUpdate(query);
                } else {
                    throw new InternalError();
                }
                
                return true;
                
            } catch (InternalError|SQLException ex){}
        } 
        return false;
    }
    
    public boolean addBettingShop(String name){
        if(connect != null){
            try {
                Statement st = connect.createStatement();
                
                String query = "INSERT INTO betting_shops(name) VALUES( \"" + name + "\")";
                st.executeUpdate(query);
                return true;
            } catch (SQLException ex){}
        }
        return false;
    }
    
    public boolean deleteBettingShop(String name){
        if(connect != null){
            try{
                String query = "PRAGMA foreign_keys = ON;"
                        + "DELETE FROM betting_shops WHERE name LIKE \"" + name + "\"";
                Statement st = connect.createStatement();
                st.executeUpdate(query);
                return true;
            } catch(SQLException ex){}
        }
        return false;
    }
    
    public boolean editRow(String[] origin, String[] destiny){
        if(connect != null){
            String id = null;
            String idT1 = null;
            String idT2 = null;
            String idFix = null;
            String idSea = null;
            
            String pos1 = null;
            String pos2 = null;
            String division = null;
            try {
                Statement st = connect.createStatement();
                String query = "SELECT id FROM teams WHERE name LIKE \"" + origin[2] + "\"";
                ResultSet rs = st.executeQuery(query);
                if(rs.next()){
                    idT1 = rs.getString("id");
                } else {
                    throw new InternalError();
                }
                query = "SELECT id FROM teams WHERE name LIKE \"" + origin[3] + "\"";
                rs = st.executeQuery(query);
                if(rs.next()){
                    idT2 = rs.getString("id");
                } else {
                    throw new InternalError();
                }
                query = "SELECT match.id as id , fixture.id as fixid, seasons.id as seaid "
                        + "FROM match "
                        + "INNER JOIN fixture on fixture.id = match.id_fixture "
                        + "INNER JOIN seasons on seasons.id = fixture.idSeason "
                        + "INNER JOIN teams as t1 on t1.id = match.team1 "
                        + "INNER JOIN teams as t2 on t2.id = match.team2 "
                        + "INNER JOIN division on (division.id_Team = match.team2) AND division.idSeason = seasons.id "
                        + "INNER JOIN leagues on leagues.id = fixture.id_League "
                        + "WHERE score1 = " + origin[0] 
                        + " AND score2 = " + origin[1]
                        + " AND t1.id = " + idT1
                        + " AND t2.id = " + idT2
                        + " AND fixture.number = " + origin[4]
                        + " AND seasons.year = " + origin[5];
                rs = st.executeQuery(query);
                if(rs.next()){
                    id = rs.getString("id");
                    idFix = rs.getString("fixid");
                    idSea = rs.getString("seaid");
                } else {
                    return false;
                }
                query = "SELECT position FROM team_fixture WHERE id_fixture = " + idFix + " AND id_Team = " + idT1;
                rs = st.executeQuery(query);
                if(rs.next()){
                    pos1 = rs.getString("position");
                } else {
                    return false;
                }
                query = "SELECT position FROM team_fixture WHERE id_fixture = " + idFix + " AND id_Team = " + idT2;
                rs = st.executeQuery(query);
                if(rs.next()){
                    pos2 = rs.getString("position");
                } else {
                    return false;
                }
                query = "SELECT div FROM division WHERE idSeason = " + idSea + " AND id_Team = " + idT1;
                rs = st.executeQuery(query);
                if(rs.next()){
                    division = rs.getString("div");
                } else {
                    return false;
                }
            }catch(Exception e){
                System.out.println(e.getMessage());
                return false;
            }
            if(deleteMatch(Integer.parseInt(origin[0]), Integer.parseInt(origin[1]), origin[2], origin[3], Integer.parseInt(origin[4]), Integer.parseInt(origin[5]))){
                Ending end[] = calculateEnding(Integer.parseInt(destiny[0]), Integer.parseInt(destiny[1]));
                if(addMatch(id, Integer.parseInt(pos1), Integer.parseInt(pos2), Integer.parseInt(destiny[0]), Integer.parseInt(destiny[1]), end[0].state, end[1].state, destiny[2], destiny[3], Integer.parseInt(destiny[4]), Integer.parseInt(destiny[5]), division)){
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false; 
    }
    
    public Ending[] calculateEnding(int score1, int score2){
        Ending[] ret = new Ending[2];
        if(score1 > score2){
            ret[0] = WIN;
            ret[1] = LOSE;
        } else if(score1 < score2){
            ret[0] = LOSE;
            ret[1] = WIN;
        } else {
            ret[0] = EVEN;
            ret[1] = EVEN;
        }
        return ret;
    }
}
