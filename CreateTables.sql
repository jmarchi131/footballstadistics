-- SQLite
PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS leagues (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name STRING NOT NULL
);

CREATE TABLE IF NOT EXISTS teams (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    id_League INTEGER,
    name STRING NOT NULL,
    FOREIGN KEY(id_League) REFERENCES leagues(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS seasons (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    year INTEGER NOT NULL,
    id_League INTEGER NOT NULL,
    FOREIGN KEY(id_League) REFERENCES leagues(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS division (
    div STRING NOT NULL,
    id_Team INTEGER,
    idSeason INTEGER,
    FOREIGN KEY(id_Team) REFERENCES teams(id) ON DELETE CASCADE,
    FOREIGN KEY(idSeason) REFERENCES seasons(id) ON DELETE CASCADE,
    PRIMARY KEY(id_Team, idSeason)
);

CREATE TABLE IF NOT EXISTS fixture (
    id INTEGER PRIMARY KEY,
    number INTEGER,
    match_day DATE NOT NULL,
    idSeason INTEGER,
    id_League INTEGER,
    FOREIGN KEY (idSeason) REFERENCES seasons(id) ON DELETE CASCADE,
    FOREIGN KEY (id_League) REFERENCES leagues(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS team_fixture (
    position INTEGER NOT NULL,
    id_Team INTEGER,
    id_fixture INTEGER,
    FOREIGN KEY (id_Team) REFERENCES teams(id) ON DELETE CASCADE,
    FOREIGN KEY (id_fixture) REFERENCES fixture(id) ON DELETE CASCADE,
    PRIMARY KEY(id_Team, id_fixture)
);

CREATE TABLE IF NOT EXISTS betting_shops (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name STRING NOT NULL
);

CREATE TABLE IF NOT EXISTS match (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    score1 INTEGER NOT NULL,
    score2 INTEGER NOT NULL,
    ending1 VARCHAR(1) NOT NULL,
    ending2 VARCHAR(1) NOT NULL,
    id_fixture INTEGER,
    team1 INTEGER,
    team2 INTEGER,
    FOREIGN KEY (id_fixture) REFERENCES fixture(id) ON DELETE CASCADE,
    FOREIGN KEY (team1) REFERENCES teams(id) ON DELETE CASCADE,
    FOREIGN KEY (team2) REFERENCES teams(id) ON DELETE CASCADE,
    CHECK(team1 != team2),
    CHECK(score1 >= 0),
    CHECK(score2 >= 0)
);

CREATE TABLE IF NOT EXISTS percentage (
    win INTEGER NOT NULL,
    even INTEGER NOT NULL,
    lose INTEGER NOT NULL,
    id_Betting INTEGER,
    id_Match INTEGER,
    FOREIGN KEY (id_Betting) REFERENCES betting_shops(id) ON DELETE CASCADE,
    FOREIGN KEY (id_Match) REFERENCES match(id) ON DELETE CASCADE,
    PRIMARY KEY (id_Betting, id_Match)
);

/*
INSERT INTO teams VALUES(1, "Real Madrid");
INSERT INTO teams VALUES(2, "Barcelona");

INSERT INTO leagues VALUES(0, "Liga Espanola");

INSERT INTO seasons VALUES(2019, 0);
INSERT INTO seasons VALUES(2002, 0);
INSERT INTO seasons VALUES(2018, 0);

INSERT INTO fixture VALUES(15, "2018-02-13", 2018);
INSERT INTO fixture VALUES(26, "2018-04-20", 2018);
INSERT INTO fixture VALUES(20, "2002-03-05", 2002);
INSERT INTO fixture VALUES(3, "2019-09-03", 2019);

INSERT INTO team_fixture VALUES(2, 1, 15);
INSERT INTO team_fixture VALUES(3, 2, 15);
INSERT into team_fixture VALUES(4, 1, 26);
INSERT INTO team_fixture VALUES(1, 2, 26);
INSERT INTO team_fixture VALUES(1, 1, 20);
INSERT INTO team_fixture VALUES(2, 2, 20);
INSERT INTO team_fixture VALUES(1, 1, 3);
INSERT INTO team_fixture VALUES(3, 2, 3);

INSERT INTO match VALUES(0, 2, 1, "G", "P", 15, 1, 2);
INSERT INTO match VALUES(1, 3, 1, "G", "P", 26, 2, 1);
INSERT INTO match VALUES(2, 2, 2, "X", "X", 20, 2, 1);
INSERT INTO match VALUES(3, 1, 4, "P", "G", 3, 1, 2);

INSERT INTO division VALUES(1, 1, 2002);
INSERT INTO division VALUES(1, 2, 2002);
INSERT INTO division VALUES(1, 1, 2018);
INSERT INTO division VALUES(1, 2, 2018);
INSERT INTO division VALUES(1, 1, 2019);
INSERT INTO division VALUES(1, 2, 2019);

DELETE FROM teams WHERE name LIKE "Barcelona";
DELETE FROM teams WHERE name LIKE "Malaga";
*/